import { ACTION_SESSION_SET } from "../actions/sessionAction"

const initialState = {
   username: "",
   translations: []
}

//return state and store some info
export const sessionReducer = (state=initialState, action) => {
    switch (action.type){
        case ACTION_SESSION_SET:
            localStorage.setItem('username', action.payload.username)
            localStorage.setItem('translations2', action.payload.translations)
            localStorage.setItem('id', action.payload.id)
            return {
                ...action.payload,
                loggedIn: true,
            }
        default:
            return state
    }
}