import {loginMiddleware} from './loginMiddleware'
import { applyMiddleware } from 'redux'
import { sessionMiddleware } from './sessionMiddleware'

export default applyMiddleware(
    loginMiddleware,
    sessionMiddleware
    )