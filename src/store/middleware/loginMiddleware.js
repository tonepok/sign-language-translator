import { LoginAPI } from "../../components/Login/LoginAPI";
import {ACTION_LOGIN_SUCCESS, loginErrorAction, loginSuccessAction} from "../actions/loginActions";
import { ACTION_LOGIN_ATTEMPTING } from "../actions/loginActions";
import { sessionSetAction } from "../actions/sessionAction";

//fetch API and login
export const loginMiddleware =
  ({ dispatch }) =>
  (next) =>
  (action) => {
    next(action);

    if (action.type === ACTION_LOGIN_ATTEMPTING) {
      LoginAPI(action.payload)
        .then((profile) => {
          dispatch(loginSuccessAction(profile));
        })
        .catch((error) => {
          dispatch(loginErrorAction(error.message));
        });
    } else if (action.type === ACTION_LOGIN_SUCCESS) {
        dispatch(sessionSetAction(action.payload))
    }
  };
