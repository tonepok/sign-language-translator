import axios from 'axios';

//Adding translations to the API and to local storage so we can access in Profile
export async function AddTranslation() {
    const translation = localStorage.getItem('sentence')
    const transList = localStorage.getItem('translations2')
    const id = localStorage.getItem('id')
    const user = localStorage.getItem('username')
    const newStringList = transList+","+translation
    const allTranslations = getArray(newStringList)
    localStorage.setItem('translations2',newStringList)

    //Was not able to clear my local storage on translations, so had to make an additional
    const newArr = localStorage.getItem('ny')
    const newWord = newArr+","+translation
    localStorage.setItem('ny',newWord)


    const translationsToApi = { 
        username: user,
        translations: allTranslations };
    return axios.put("http://localhost:8007/users/"+id, translationsToApi)
        .then(response => response.data)
        .catch(error => {
            console.error('Something went wrong!', error);
          });
    }

//translates string to array
  function getArray(words){
    let word=""
    const wordsArray = []

    for (let i=0; i<words.length; i++){
       if (words[i] === ','){
           let copy = word;
           wordsArray.push(copy)
           word=""
       }
       else {
           word+=words[i]
       }
    }
    let copy = word;
    wordsArray.push(copy)
    return wordsArray
  }

