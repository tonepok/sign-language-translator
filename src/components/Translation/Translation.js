import { Form,InputGroup, FormControl, Button } from "react-bootstrap";
import { useState } from 'react'
import AppContainer from "../../hoc/AppContainer";
import { Redirect } from 'react-router-dom'
import { useSelector } from 'react-redux'
import './Translation.css';
import {AddTranslation} from './TranslationsAPI.js';

//Translates what the user unputs so corresponding sign images
const Translation = () => {
    const { loggedIn } = useSelector(state => state.sessionReducer)
    const [ translation, setTranslation] = useState({
        sentence: '',
    })
    const [html, setHtml] = useState({
        imgHtml: '',
    })
    const onInputChange = event => {
        setTranslation({
            ...translation,
            [event.target.id]: event.target.value
        })
     }

     //triggered on button click and translates to HTML-tags so user can see images
    const translate = event => {
        event.preventDefault() 
        const sentence = translation.sentence
        let imgList = ""
        for (let i=0; i<sentence.length; i++) {
            imgList+= "<li><img src='./signs/"+sentence[i]+".png' alt=''/></li>"
        }
        localStorage.setItem('sentence', sentence)
        AddTranslation()
        setHtml({
            imgHtml: imgList
        })
    }

    return (
        <main>
            {!loggedIn && <Redirect to="/"/>}
            <h4>Write in the box to translate to sign language</h4>
            <AppContainer>
            <Form onSubmit={ translate }>
                <InputGroup className="mb-3">
                <InputGroup.Text>
                <span className="material-icons">keyboard </span>
                </InputGroup.Text>
                    <FormControl
                    type="text"
                    placeholder="What do you want translated?"
                    className="form-control"
                    id="sentence"
                    onChange={onInputChange}
                    />
                    <Button variant="outline-secondary" id="button-addon2" type="submit">
                    <span class="material-icons">
                    keyboard_double_arrow_right
                    </span>
                    </Button>
                </InputGroup>
                </Form>
                <div className="myBox">
                    <div className="translation">
                        <ul>
                            <div dangerouslySetInnerHTML={{ __html: html.imgHtml }} />
                        </ul>
                    </div>
                </div>
            </AppContainer>
        </main>
    )
}

export default Translation