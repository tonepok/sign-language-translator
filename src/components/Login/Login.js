import { useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { Redirect } from 'react-router-dom'
import AppContainer from '../../hoc/AppContainer'
import { loginAttemptAction } from '../../store/actions/loginActions'
import { Form, InputGroup, FormControl, Button} from "react-bootstrap";
import './Login.css';

//tries to log in on click
const Login = () => {
    const dispatch = useDispatch()
    const { loginError, loginAttempting } = useSelector(state => state.loginReducer)
    const { loggedIn } = useSelector(state => state.sessionReducer)
    const [ credentials, setCredentials] = useState({
        username: '',
    })
    const onInputChange = event => {
       setCredentials({
           ...credentials,
           [event.target.id]: event.target.value
       })
    }

    const onFormSubmit = event => {
        event.preventDefault() 
        dispatch(loginAttemptAction(credentials))
    }

    return (
        <main>
        {loggedIn && <Redirect to="/translation" />}
        {!loggedIn &&
            <AppContainer>
            <div className="mt-3 mb-3">
                <img className="image" src={'./bkg3.png'} alt="Bakground of signs saying Sign" />
                <div className="loginBox">        
                <Form onSubmit={ onFormSubmit }>

                <InputGroup className="mb-3">
                <InputGroup.Text>
                <span className="material-icons">auto_awesome</span>
                </InputGroup.Text>
                    <FormControl
                    id="username"
                    type="text"
                    placeholder="Enter your name"
                    aria-label="Enter your name"
                    className="form-control"
                    aria-describedby="basic-addon2"
                    onChange={onInputChange}
                    />
                    <Button variant="outline-secondary" id="button-addon2"
                    type="submit">
                    Login
                    </Button>
                </InputGroup>
                </Form>
                </div>

            </div>
            { loginAttempting &&
                <p>Attempting to login...</p>
            }

            { loginError &&
                <div className="alert alert-danger" role="alert">
                    <h4>Login failed!</h4>
                    <p className="mb-0">{ loginError }</p>
                </div>}
        </AppContainer>
        }
        </main>
    )
}

export default Login