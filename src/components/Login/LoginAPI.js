/* checks if user already is registered, in that case log in to that.
If not create a new user */
export async function LoginAPI(theUsername) {
    const username = theUsername.username
    const existingUser = await checkUser(username);
    if (existingUser.length) {
      return new Promise((resolve) => resolve(existingUser.pop()));
    }
  
    return fetch("http://localhost:8007/users", {
      method: "POST",
      headers: {
          'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        username,
        translations: [],
      }),
    }).then(handleFirstResponse);
  }
  
  async function checkUser(username) {
    return fetch("http://localhost:8007/users?username=" + username)
    .then(handleFirstResponse)
  }
  
  function handleFirstResponse(response) {
    return response.json();
  }
  