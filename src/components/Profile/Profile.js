import AppContainer from '../../hoc/AppContainer'
import { Button} from "react-bootstrap";
import axios from 'axios';
import './Profile.css';

/* Shows recents translations. Had trouble clearing the local storage so I use two variables
to keep track, and one boolean to check if user has cleared earlier this session */
const Profile = () => {
    let completeArray = []
    let words2 = localStorage.getItem('ny')
    if (words2==null){
        words2=""
    }
    const iscleared = localStorage.getItem('iscleared')
    if (!iscleared){
        let words = localStorage.getItem('translations2')
        if (words==null){
            words=""
        }
        let wordsArr = words.split(",");
        const len = wordsArr.length
        for (let i=0; i<len; i++){
            completeArray.push(wordsArr[i])
        }
    }
    const words2Arr = words2.split(",");
    const len2 = words2Arr.length
    for (let i=1; i<len2; i++){
        completeArray.push(words2Arr[i])
    }

    let htmlList = ""
    for (let i=0; i<completeArray.length; i++) {
        htmlList+= "<li><h3>"+completeArray[i]+"</h3></li>"
        if (i>=10){
            break;
        }
    }
    //clears API transactions and local storage
    const clearHistory = () => {
        const id = localStorage.getItem('id')
        const user = localStorage.getItem('username')
        localStorage.setItem('iscleared', true)
        localStorage.setItem('translations2',"")
        localStorage.setItem('ny',"")
        htmlList=""

        const translationsToApi = { 
            username: user,
            translations: []};
        console.log(translationsToApi)
        return axios.put("http://localhost:8007/users/"+id, translationsToApi)
            .then(response => response.data)
            .catch(error => {
                console.error('Something went wrong!', error);
              }).then(window.location.reload())
    }

    return (
        <main className="Profile">
            <AppContainer>
            <h1>Profile page</h1>
            <div class="infoBox">
            <p>Your most recent translations:</p>
            <div className="myBox2">
                    <div className="translations">
                        <ul>
                            <div dangerouslySetInnerHTML={{ __html: htmlList }} />
                        </ul>
                    </div>
                </div>
                </div>
            <Button variant="outline-secondary" id="button-addon2" onClick={ clearHistory }>
                Clear history
                </Button>
            </AppContainer>
        </main>
    )
}

export default Profile