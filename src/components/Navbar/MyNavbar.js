import React from 'react'
import { Navbar, Nav, NavDropdown } from 'react-bootstrap';
import AppContainer from '../../hoc/AppContainer';
import './MyNavbar.css';
import { useSelector } from 'react-redux'

const logout = () => {
    localStorage.clear();
    window.location.href = '/';
}

//Navbar with div links
const MyNavbar = () => {
    const { loggedIn } = useSelector(state => state.sessionReducer)
    const userDetails = localStorage.getItem('username');
    return(
        <Navbar bg="light" expand="lg" className='mb-4'>
        <AppContainer>
        <Nav.Item>
            <span className="material-icons">volunteer_activism</span>
            <Navbar.Brand href="/translation">Sign Language Translator</Navbar.Brand>
            </Nav.Item>

            <Nav className="justify-content-end" activeKey="/home">
            <Nav.Item>

            {loggedIn && (<Nav>
                <span class="material-icons">
                person_outline
                </span>
               <NavDropdown title={userDetails}>
                <NavDropdown.Item href="/profile">Profile</NavDropdown.Item>
                <NavDropdown.Item href="/translation">Translate</NavDropdown.Item>
                <NavDropdown.Item onClick={ logout }>Logout</NavDropdown.Item>
               </NavDropdown>
            </Nav>)}
             
            {!loggedIn && (<Nav.Link>Login</Nav.Link>)}
            </Nav.Item>
            </Nav>
        </AppContainer>
        </Navbar>
    )
}

export default MyNavbar;