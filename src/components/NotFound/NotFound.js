import {Link} from 'react-router-dom'
import AppContainer from '../../hoc/AppContainer'

const NotFound = () => {
    return (
        <main className="NotFound">
            <AppContainer>
            <h1>Hmm.. Where are you trying to go?</h1>
            <p>This page does not exist...</p>
            <Link to="/">Take me home</Link>
            </AppContainer>
        </main>
    )
}

export default NotFound