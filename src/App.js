import './App.css';
import {
  BrowserRouter,
  Switch,
  Route,
  //Redirect
} from 'react-router-dom'
import Login from './components/Login/Login'
import Translation from './components/Translation/Translation'
import NotFound from './components/NotFound/NotFound'
import MyNavbar from './components/Navbar/MyNavbar';
import React from 'react'
import Profile from './components/Profile/Profile';

function App() {
  return (
    <BrowserRouter>
    <link rel="preconnect" href="https://fonts.googleapis.com"/>
    <link href="https://fonts.googleapis.com/css2?family=Fugaz+One&family=Vollkorn+SC&display=swap" rel="stylesheet"/>
    <link href="https://fonts.googleapis.com/css2?family=Kaisei+Tokumin&display=swap" rel="stylesheet"/>
    <MyNavbar/>
      <div className="App">
        <Switch>
          <Route path="/" exact component={Login}/>
          <Route path="/translation" component={Translation}/>
          <Route path="/profile" component={ Profile }/>
          <Route path="*" component={ NotFound }/>
        </Switch>
      </div>
    </BrowserRouter>
  );
}

export default App;
