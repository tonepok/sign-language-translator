# Sign Language Translator Application
This app lets you translate a text to american sign language.
You have to register with a username to use the app.
You can also see your 10 most recent translations, and delete your history.

## Before you run:
Before you start you should run the local slt.json file on port 8007.
If that port is occupied, you can change the code in the files: LoginAPI.js, TranslationsAPI.js and Profile.js to another port.
This json-file contains the database of users and their texts (translations)

## How to run:
cd my-app      
npm start  
